﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace collectdSrv.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("collectdSrv.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Configuration {
            get {
                object obj = ResourceManager.GetObject("Configuration", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {
        ///
        ///  &quot;counters&quot;: [
        ///    {
        ///      &quot;name&quot;: &quot;Web Service&quot;,
        ///      &quot;counterName&quot;: [ &quot;Bytes Received/sec&quot;, &quot;Bytes Sent/sec&quot;, &quot;Connection Attempts/sec&quot;, &quot;Current Connections&quot;, &quot;Current Blocked Async I/O Requests&quot;, &quot;Current Connections&quot;, &quot;Total Options Requests&quot;, &quot;Total Put Requests&quot;, &quot;Total Post Requests&quot;, &quot;Total Get Requests&quot;, &quot;Total Delete Requests&quot; ]
        ///    },
        ///    {
        ///      &quot;name&quot;: &quot;Processor&quot;,
        ///      &quot;counterName&quot;: [ &quot;% Processor Time&quot;, &quot;% User Time&quot;, &quot;% Privileged Time&quot;, &quot;Interrupts/sec&quot;, &quot;% DPC Time&quot;,&quot;% Interrupt Ti [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ExternalConstants {
            get {
                return ResourceManager.GetString("ExternalConstants", resourceCulture);
            }
        }
    }
}
